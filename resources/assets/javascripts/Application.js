//VENDORS
window.$ = window.jQuery = require('jquery');
let AOS = require('aos');
require('magnific-popup');

class Application{
    constructor(){
        this._init();
    }

    _init(){
      /*$(window).on('load', function() {
        AOS.init();
      });*/
      $(window).on('load', () => {
        setTimeout( () => {
        AOS.init({
          disable: function () {
            // let maxWidth = 992;
            // return window.innerWidth < maxWidth;
          }
        });
        }, 300)
      })
      this._lineAnimate();
      this._videoPopup();
    }

    _lineAnimate(){
      setTimeout(function () {
        $('.decore-line').addClass('animate');
      }, 3000)
    }

    _videoPopup(){
      $('.popup-youtube').magnificPopup({
        items: {
          src: 'https://www.youtube.com/watch?v=xcJtL7QggTI'
        },
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
      });
    }
}

new Application();
